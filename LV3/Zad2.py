import matplotlib.pyplot as plt
import pandas as pd


cars = pd.read_csv("LV3/mtcars.csv")

# 1
#pd.DataFrame(cars[(cars.cyl == 4) | (cars.cyl == 6) | (cars.cyl == 8)].mpg).plot.bar()

# 2
#cars.plot.box(column="wt", by="cyl")

# 3
#cars.plot(column="mpg", by="am")

# 4
fig = plt.figure()
ax = fig.add_subplot()
ax.scatter(cars[cars.am == 0].qsec, cars[cars.am == 0].hp, color="gray", label="Manual")
ax.scatter(cars[cars.am == 1].qsec, cars[cars.am == 1].hp, color="orange", label="Auto")
ax.legend(loc="upper right")


plt.show()
