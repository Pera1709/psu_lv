import keras


train_ds = keras.utils.image_dataset_from_directory(
    directory='LV8/gtsrb_dataset/Train/',
    labels='inferred',
    label_mode='categorical',
    batch_size=32,
    image_size=(48, 48))

test_ds = keras.utils.image_dataset_from_directory(
    directory='LV8/gtsrb_dataset/Test.dir/',
    labels='inferred',
    label_mode='categorical',
    batch_size=1,
    image_size=(48, 48))
 
inputs = keras.Input(shape=(48,48,3))
x = keras.layers.Rescaling(1./255)(inputs)
x = keras.layers.Conv2D(32, kernel_size=(3, 3), padding='same', activation="relu")(x)
x = keras.layers.Conv2D(32, kernel_size=(3, 3), padding='valid', activation="relu")(x)
x = keras.layers.MaxPool2D(pool_size = (2, 2), strides = (2, 2))(x)
x = keras.layers.Dropout(rate=0.2)(x)
x = keras.layers.Conv2D(64, kernel_size=(3, 3), padding='same', activation="relu")(x)
x = keras.layers.Conv2D(64, kernel_size=(3, 3), padding='valid', activation="relu")(x)
x = keras.layers.MaxPool2D(pool_size=(2,2), strides=(2, 2))(x)
x = keras.layers.Dropout(rate=0.2)(x)
x = keras.layers.Conv2D(128, kernel_size=(3, 3), padding='same', activation="relu")(x)
x = keras.layers.Conv2D(128, kernel_size=(3, 3), padding='valid', activation="relu")(x)
x = keras.layers.MaxPool2D(pool_size=(2,2), strides=(2, 2))(x)
x = keras.layers.Dropout(rate=0.2)(x)

x = keras.layers.Flatten()(x)
x = keras.layers.Dense(512, activation="relu")(x)
x = keras.layers.Dropout(rate=0.5)(x)
outputs = keras.layers.Dense(43, activation="softmax")(x)
 
model = keras.Model(inputs=inputs, outputs=outputs, name="gtsrb_model")
model.summary()
 
history = model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
 
history = model.fit(train_ds, epochs=5, batch_size=32)
 
score = model.evaluate(test_ds, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])
