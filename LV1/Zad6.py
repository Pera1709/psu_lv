
try:
    file = open(input("File name: "))
except:
    print("Files doesn't exist!")
    exit()

mails = []
hostnames = dict()

for line in file:
    words = line.split()

    if (len(words) > 0 and words[0] == "From"):
        mail = words[1]
        mails.append(mail)

        hostname = mail[mail.find('@') + 1:-1]
        
        if (hostname in hostnames):
            hostnames[hostname] += 1
        else:
            hostnames[hostname] = 1

print(mails)
print(hostnames)
