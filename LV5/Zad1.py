from funkcija_5_1 import *
import sklearn.cluster as clstr
import matplotlib.pylab as plt


data = generate_data(500, 1)

model = clstr.KMeans(n_clusters=3)
model.fit(data)
fitData = model.predict(data)

plt.figure()
plt.scatter(data[:,0], data[:,1], c = fitData)
plt.scatter(model.cluster_centers_[:,0], model.cluster_centers_[:,1], c='red', marker='x')
plt.show()
